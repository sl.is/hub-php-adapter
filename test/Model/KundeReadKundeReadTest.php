<?php
/**
 * KundeReadKundeReadTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  SLIS\Adapter\Hub
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * slis hub
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.7.4
 * Contact: dev@slis.io
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.1.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace SLIS\Adapter\Hub\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * KundeReadKundeReadTest Class Doc Comment
 *
 * @category    Class
 * @description KundeReadKundeRead
 * @package     SLIS\Adapter\Hub
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class KundeReadKundeReadTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "KundeReadKundeRead"
     */
    public function testKundeReadKundeRead()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "nummer"
     */
    public function testPropertyNummer()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "kennzeichen"
     */
    public function testPropertyKennzeichen()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "bsnr"
     */
    public function testPropertyBsnr()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "institutionskennzeichen"
     */
    public function testPropertyInstitutionskennzeichen()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "adresse"
     */
    public function testPropertyAdresse()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "rechungsadresse"
     */
    public function testPropertyRechungsadresse()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "standorte"
     */
    public function testPropertyStandorte()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "benutzer"
     */
    public function testPropertyBenutzer()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "kategorien"
     */
    public function testPropertyKategorien()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "bestellungen"
     */
    public function testPropertyBestellungen()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "uuid"
     */
    public function testPropertyUuid()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "created"
     */
    public function testPropertyCreated()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "updated"
     */
    public function testPropertyUpdated()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
