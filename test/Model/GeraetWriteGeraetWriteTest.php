<?php
/**
 * GeraetWriteGeraetWriteTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  SLIS\Adapter\Hub
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * slis hub
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.7.4
 * Contact: dev@slis.io
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.1.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace SLIS\Adapter\Hub\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * GeraetWriteGeraetWriteTest Class Doc Comment
 *
 * @category    Class
 * @description GeraetWriteGeraetWrite
 * @package     SLIS\Adapter\Hub
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class GeraetWriteGeraetWriteTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "GeraetWriteGeraetWrite"
     */
    public function testGeraetWriteGeraetWrite()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "ip"
     */
    public function testPropertyIp()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "mac"
     */
    public function testPropertyMac()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "typ"
     */
    public function testPropertyTyp()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "beschreibung"
     */
    public function testPropertyBeschreibung()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "seriennummer"
     */
    public function testPropertySeriennummer()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "version"
     */
    public function testPropertyVersion()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "externalIdentifier"
     */
    public function testPropertyExternalIdentifier()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "netzwerk"
     */
    public function testPropertyNetzwerk()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "hersteller"
     */
    public function testPropertyHersteller()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "tags"
     */
    public function testPropertyTags()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "uuid"
     */
    public function testPropertyUuid()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
