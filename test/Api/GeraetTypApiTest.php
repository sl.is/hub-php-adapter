<?php
/**
 * GeraetTypApiTest
 * PHP version 7.4
 *
 * @category Class
 * @package  SLIS\Adapter\Hub
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * slis hub
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.7.4
 * Contact: dev@slis.io
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.1.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SLIS\Adapter\Hub\Test\Api;

use \SLIS\Adapter\Hub\Configuration;
use \SLIS\Adapter\Hub\ApiException;
use \SLIS\Adapter\Hub\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * GeraetTypApiTest Class Doc Comment
 *
 * @category Class
 * @package  SLIS\Adapter\Hub
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class GeraetTypApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for deleteGeraetTypItem
     *
     * Removes the GeraetTyp resource..
     *
     */
    public function testDeleteGeraetTypItem()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getGeraetTypCollection
     *
     * Retrieves the collection of GeraetTyp resources..
     *
     */
    public function testGetGeraetTypCollection()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getGeraetTypItem
     *
     * Retrieves a GeraetTyp resource..
     *
     */
    public function testGetGeraetTypItem()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postGeraetTypCollection
     *
     * Creates a GeraetTyp resource..
     *
     */
    public function testPostGeraetTypCollection()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
