<?php
/**
 * StatusApiTest
 * PHP version 7.4
 *
 * @category Class
 * @package  SLIS\Adapter\Hub
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * slis hub
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.7.4
 * Contact: dev@slis.io
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.1.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SLIS\Adapter\Hub\Test\Api;

use \SLIS\Adapter\Hub\Configuration;
use \SLIS\Adapter\Hub\ApiException;
use \SLIS\Adapter\Hub\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * StatusApiTest Class Doc Comment
 *
 * @category Class
 * @package  SLIS\Adapter\Hub
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class StatusApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for getStatusCollection
     *
     * Retrieves the collection of Status resources..
     *
     */
    public function testGetStatusCollection()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getStatusItem
     *
     * Retrieves a Status resource..
     *
     */
    public function testGetStatusItem()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
