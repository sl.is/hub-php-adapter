# SLIS\Adapter\Hub\BestellungApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiKundesBestellungensGetSubresourceKundeSubresource()**](BestellungApi.md#apiKundesBestellungensGetSubresourceKundeSubresource) | **GET** /kundes/{uuid}/bestellungens | Retrieves a Kunde resource. |
| [**deleteBestellungItem()**](BestellungApi.md#deleteBestellungItem) | **DELETE** /bestellung/{uuid} | Removes the Bestellung resource. |
| [**getAvaliblePositionsBestellungCollection()**](BestellungApi.md#getAvaliblePositionsBestellungCollection) | **GET** /bestellung/positionen/_available | Get a list of all available order positions |
| [**getBestellungCollection()**](BestellungApi.md#getBestellungCollection) | **GET** /bestellung | Retrieves the collection of Bestellung resources. |
| [**getBestellungItem()**](BestellungApi.md#getBestellungItem) | **GET** /bestellung/{uuid} | Retrieves a Bestellung resource. |
| [**patchBestellungItem()**](BestellungApi.md#patchBestellungItem) | **PATCH** /bestellung/{uuid} | Updates the Bestellung resource. |
| [**postBestellungCollection()**](BestellungApi.md#postBestellungCollection) | **POST** /bestellung | Creates a Bestellung resource. |


## `apiKundesBestellungensGetSubresourceKundeSubresource()`

```php
apiKundesBestellungensGetSubresourceKundeSubresource($uuid, $page): \SLIS\Adapter\Hub\Model\GetBestellungCollection200Response
```

Retrieves a Kunde resource.

Retrieves a Kunde resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BestellungApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Kunde identifier
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->apiKundesBestellungensGetSubresourceKundeSubresource($uuid, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BestellungApi->apiKundesBestellungensGetSubresourceKundeSubresource: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Kunde identifier | |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetBestellungCollection200Response**](../Model/GetBestellungCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteBestellungItem()`

```php
deleteBestellungItem($uuid)
```

Removes the Bestellung resource.

Removes the Bestellung resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BestellungApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteBestellungItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling BestellungApi->deleteBestellungItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAvaliblePositionsBestellungCollection()`

```php
getAvaliblePositionsBestellungCollection($page): \SLIS\Adapter\Hub\Model\GetBestellungCollection200Response
```

Get a list of all available order positions

You'll find the available order positions with name and example data

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BestellungApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getAvaliblePositionsBestellungCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BestellungApi->getAvaliblePositionsBestellungCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetBestellungCollection200Response**](../Model/GetBestellungCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getBestellungCollection()`

```php
getBestellungCollection($page): \SLIS\Adapter\Hub\Model\GetBestellungCollection200Response
```

Retrieves the collection of Bestellung resources.

Retrieves the collection of Bestellung resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BestellungApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getBestellungCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BestellungApi->getBestellungCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetBestellungCollection200Response**](../Model/GetBestellungCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getBestellungItem()`

```php
getBestellungItem($uuid): \SLIS\Adapter\Hub\Model\BestellungJsonldRead
```

Retrieves a Bestellung resource.

Retrieves a Bestellung resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BestellungApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getBestellungItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BestellungApi->getBestellungItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\BestellungJsonldRead**](../Model/BestellungJsonldRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `patchBestellungItem()`

```php
patchBestellungItem($uuid, $bestellungPatch): \SLIS\Adapter\Hub\Model\BestellungJsonldRead
```

Updates the Bestellung resource.

Updates the Bestellung resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BestellungApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier
$bestellungPatch = new \SLIS\Adapter\Hub\Model\BestellungPatch(); // \SLIS\Adapter\Hub\Model\BestellungPatch | The updated Bestellung resource

try {
    $result = $apiInstance->patchBestellungItem($uuid, $bestellungPatch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BestellungApi->patchBestellungItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |
| **bestellungPatch** | [**\SLIS\Adapter\Hub\Model\BestellungPatch**](../Model/BestellungPatch.md)| The updated Bestellung resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\BestellungJsonldRead**](../Model/BestellungJsonldRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/merge-patch+json`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postBestellungCollection()`

```php
postBestellungCollection($bestellungJsonldCreate): \SLIS\Adapter\Hub\Model\BestellungJsonldRead
```

Creates a Bestellung resource.

Creates a Bestellung resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BestellungApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$bestellungJsonldCreate = new \SLIS\Adapter\Hub\Model\BestellungJsonldCreate(); // \SLIS\Adapter\Hub\Model\BestellungJsonldCreate | The new Bestellung resource

try {
    $result = $apiInstance->postBestellungCollection($bestellungJsonldCreate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BestellungApi->postBestellungCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **bestellungJsonldCreate** | [**\SLIS\Adapter\Hub\Model\BestellungJsonldCreate**](../Model/BestellungJsonldCreate.md)| The new Bestellung resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\BestellungJsonldRead**](../Model/BestellungJsonldRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
