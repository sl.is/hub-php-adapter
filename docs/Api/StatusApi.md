# SLIS\Adapter\Hub\StatusApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**getStatusCollection()**](StatusApi.md#getStatusCollection) | **GET** /statuses | Retrieves the collection of Status resources. |
| [**getStatusItem()**](StatusApi.md#getStatusItem) | **GET** /statuses/{uuid} | Retrieves a Status resource. |


## `getStatusCollection()`

```php
getStatusCollection($page, $name, $type, $type2): \SLIS\Adapter\Hub\Model\GetStatusCollection200Response
```

Retrieves the collection of Status resources.

Retrieves the collection of Status resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\StatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number
$name = 'name_example'; // string | 
$type = 'type_example'; // string | 
$type2 = array('type_example'); // string[] | 

try {
    $result = $apiInstance->getStatusCollection($page, $name, $type, $type2);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusApi->getStatusCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |
| **name** | **string**|  | [optional] |
| **type** | **string**|  | [optional] |
| **type2** | [**string[]**](../Model/string.md)|  | [optional] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetStatusCollection200Response**](../Model/GetStatusCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getStatusItem()`

```php
getStatusItem($uuid): \SLIS\Adapter\Hub\Model\StatusJsonldReadStatusRead
```

Retrieves a Status resource.

Retrieves a Status resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\StatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getStatusItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusApi->getStatusItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\StatusJsonldReadStatusRead**](../Model/StatusJsonldReadStatusRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
