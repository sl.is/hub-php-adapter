# SLIS\Adapter\Hub\FirewallApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource()**](FirewallApi.md#apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource) | **GET** /firewalls/{uuid}/service_level_agreement | Retrieves a Firewall resource. |
| [**deleteFirewallItem()**](FirewallApi.md#deleteFirewallItem) | **DELETE** /firewalls/{uuid} | Removes the Firewall resource. |
| [**getFirewallCollection()**](FirewallApi.md#getFirewallCollection) | **GET** /firewalls | Retrieves the collection of Firewall resources. |
| [**getFirewallItem()**](FirewallApi.md#getFirewallItem) | **GET** /firewalls/{uuid} | Retrieves a Firewall resource. |
| [**postFirewallCollection()**](FirewallApi.md#postFirewallCollection) | **POST** /firewalls | Creates a Firewall resource. |
| [**putFirewallItem()**](FirewallApi.md#putFirewallItem) | **PUT** /firewalls/{uuid} | Replaces the Firewall resource. |


## `apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource()`

```php
apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource($uuid): \SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead
```

Retrieves a Firewall resource.

Retrieves a Firewall resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\FirewallApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Firewall identifier

try {
    $result = $apiInstance->apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FirewallApi->apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Firewall identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead**](../Model/ServiceLevelAgreementJsonldReadSlaRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteFirewallItem()`

```php
deleteFirewallItem($uuid)
```

Removes the Firewall resource.

Removes the Firewall resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\FirewallApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteFirewallItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling FirewallApi->deleteFirewallItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getFirewallCollection()`

```php
getFirewallCollection($page, $externalIdentifier, $externalIdentifier2, $ip, $ip2, $mac, $seriennummer, $seriennummer2, $tags, $tags2): \SLIS\Adapter\Hub\Model\GetFirewallCollection200Response
```

Retrieves the collection of Firewall resources.

Retrieves the collection of Firewall resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\FirewallApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number
$externalIdentifier = 'externalIdentifier_example'; // string | 
$externalIdentifier2 = array('externalIdentifier_example'); // string[] | 
$ip = 'ip_example'; // string | 
$ip2 = array('ip_example'); // string[] | 
$mac = 'mac_example'; // string | 
$seriennummer = 'seriennummer_example'; // string | 
$seriennummer2 = array('seriennummer_example'); // string[] | 
$tags = 'tags_example'; // string | 
$tags2 = array('tags_example'); // string[] | 

try {
    $result = $apiInstance->getFirewallCollection($page, $externalIdentifier, $externalIdentifier2, $ip, $ip2, $mac, $seriennummer, $seriennummer2, $tags, $tags2);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FirewallApi->getFirewallCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |
| **externalIdentifier** | **string**|  | [optional] |
| **externalIdentifier2** | [**string[]**](../Model/string.md)|  | [optional] |
| **ip** | **string**|  | [optional] |
| **ip2** | [**string[]**](../Model/string.md)|  | [optional] |
| **mac** | **string**|  | [optional] |
| **seriennummer** | **string**|  | [optional] |
| **seriennummer2** | [**string[]**](../Model/string.md)|  | [optional] |
| **tags** | **string**|  | [optional] |
| **tags2** | [**string[]**](../Model/string.md)|  | [optional] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetFirewallCollection200Response**](../Model/GetFirewallCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getFirewallItem()`

```php
getFirewallItem($uuid): \SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallRead
```

Retrieves a Firewall resource.

Retrieves a Firewall resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\FirewallApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getFirewallItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FirewallApi->getFirewallItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallRead**](../Model/FirewallJsonldReadFirewallRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postFirewallCollection()`

```php
postFirewallCollection($firewallJsonldWriteFirewallWrite): \SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallRead
```

Creates a Firewall resource.

Creates a Firewall resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\FirewallApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$firewallJsonldWriteFirewallWrite = new \SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWrite(); // \SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWrite | The new Firewall resource

try {
    $result = $apiInstance->postFirewallCollection($firewallJsonldWriteFirewallWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FirewallApi->postFirewallCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **firewallJsonldWriteFirewallWrite** | [**\SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWrite**](../Model/FirewallJsonldWriteFirewallWrite.md)| The new Firewall resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallRead**](../Model/FirewallJsonldReadFirewallRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putFirewallItem()`

```php
putFirewallItem($uuid, $firewallJsonldWriteFirewallWrite): \SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallRead
```

Replaces the Firewall resource.

Replaces the Firewall resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\FirewallApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier
$firewallJsonldWriteFirewallWrite = new \SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWrite(); // \SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWrite | The updated Firewall resource

try {
    $result = $apiInstance->putFirewallItem($uuid, $firewallJsonldWriteFirewallWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FirewallApi->putFirewallItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |
| **firewallJsonldWriteFirewallWrite** | [**\SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWrite**](../Model/FirewallJsonldWriteFirewallWrite.md)| The updated Firewall resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallRead**](../Model/FirewallJsonldReadFirewallRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
