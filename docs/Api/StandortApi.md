# SLIS\Adapter\Hub\StandortApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiStandortsNetzwerkesGetSubresourceStandortSubresource()**](StandortApi.md#apiStandortsNetzwerkesGetSubresourceStandortSubresource) | **GET** /standorts/{uuid}/netzwerkes | Retrieves a Standort resource. |
| [**deleteStandortItem()**](StandortApi.md#deleteStandortItem) | **DELETE** /standorte/{uuid} | Removes the Standort resource. |
| [**getStandortCollection()**](StandortApi.md#getStandortCollection) | **GET** /standorte | Retrieves the collection of Standort resources. |
| [**getStandortItem()**](StandortApi.md#getStandortItem) | **GET** /standorte/{uuid} | Retrieves a Standort resource. |
| [**postStandortCollection()**](StandortApi.md#postStandortCollection) | **POST** /standorte | Creates a Standort resource. |


## `apiStandortsNetzwerkesGetSubresourceStandortSubresource()`

```php
apiStandortsNetzwerkesGetSubresourceStandortSubresource($uuid, $page): \SLIS\Adapter\Hub\Model\GetNetzwerkCollection200Response
```

Retrieves a Standort resource.

Retrieves a Standort resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\StandortApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Standort identifier
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->apiStandortsNetzwerkesGetSubresourceStandortSubresource($uuid, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StandortApi->apiStandortsNetzwerkesGetSubresourceStandortSubresource: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Standort identifier | |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetNetzwerkCollection200Response**](../Model/GetNetzwerkCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteStandortItem()`

```php
deleteStandortItem($uuid)
```

Removes the Standort resource.

Removes the Standort resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\StandortApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteStandortItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling StandortApi->deleteStandortItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getStandortCollection()`

```php
getStandortCollection($page): \SLIS\Adapter\Hub\Model\GetStandortCollection200Response
```

Retrieves the collection of Standort resources.

Retrieves the collection of Standort resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\StandortApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getStandortCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StandortApi->getStandortCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetStandortCollection200Response**](../Model/GetStandortCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getStandortItem()`

```php
getStandortItem($uuid): \SLIS\Adapter\Hub\Model\StandortJsonldWriteStandortRead
```

Retrieves a Standort resource.

Retrieves a Standort resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\StandortApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getStandortItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StandortApi->getStandortItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\StandortJsonldWriteStandortRead**](../Model/StandortJsonldWriteStandortRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postStandortCollection()`

```php
postStandortCollection($standortJsonldReadStandortWrite): \SLIS\Adapter\Hub\Model\StandortJsonldWriteStandortRead
```

Creates a Standort resource.

Creates a Standort resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\StandortApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$standortJsonldReadStandortWrite = new \SLIS\Adapter\Hub\Model\StandortJsonldReadStandortWrite(); // \SLIS\Adapter\Hub\Model\StandortJsonldReadStandortWrite | The new Standort resource

try {
    $result = $apiInstance->postStandortCollection($standortJsonldReadStandortWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StandortApi->postStandortCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **standortJsonldReadStandortWrite** | [**\SLIS\Adapter\Hub\Model\StandortJsonldReadStandortWrite**](../Model/StandortJsonldReadStandortWrite.md)| The new Standort resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\StandortJsonldWriteStandortRead**](../Model/StandortJsonldWriteStandortRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
