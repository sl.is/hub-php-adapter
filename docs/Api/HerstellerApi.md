# SLIS\Adapter\Hub\HerstellerApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteHerstellerItem()**](HerstellerApi.md#deleteHerstellerItem) | **DELETE** /herstellers/{uuid} | Removes the Hersteller resource. |
| [**getHerstellerCollection()**](HerstellerApi.md#getHerstellerCollection) | **GET** /herstellers | Retrieves the collection of Hersteller resources. |
| [**getHerstellerItem()**](HerstellerApi.md#getHerstellerItem) | **GET** /herstellers/{uuid} | Retrieves a Hersteller resource. |
| [**postHerstellerCollection()**](HerstellerApi.md#postHerstellerCollection) | **POST** /herstellers | Creates a Hersteller resource. |
| [**putHerstellerItem()**](HerstellerApi.md#putHerstellerItem) | **PUT** /herstellers/{uuid} | Replaces the Hersteller resource. |


## `deleteHerstellerItem()`

```php
deleteHerstellerItem($uuid)
```

Removes the Hersteller resource.

Removes the Hersteller resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\HerstellerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteHerstellerItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling HerstellerApi->deleteHerstellerItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getHerstellerCollection()`

```php
getHerstellerCollection($page): \SLIS\Adapter\Hub\Model\GetHerstellerCollection200Response
```

Retrieves the collection of Hersteller resources.

Retrieves the collection of Hersteller resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\HerstellerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getHerstellerCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HerstellerApi->getHerstellerCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetHerstellerCollection200Response**](../Model/GetHerstellerCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getHerstellerItem()`

```php
getHerstellerItem($uuid): \SLIS\Adapter\Hub\Model\HerstellerJsonldReadHerstellerRead
```

Retrieves a Hersteller resource.

Retrieves a Hersteller resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\HerstellerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getHerstellerItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HerstellerApi->getHerstellerItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\HerstellerJsonldReadHerstellerRead**](../Model/HerstellerJsonldReadHerstellerRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postHerstellerCollection()`

```php
postHerstellerCollection($herstellerJsonldWriteHerstellerWrite): \SLIS\Adapter\Hub\Model\HerstellerJsonldReadHerstellerRead
```

Creates a Hersteller resource.

Creates a Hersteller resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\HerstellerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$herstellerJsonldWriteHerstellerWrite = new \SLIS\Adapter\Hub\Model\HerstellerJsonldWriteHerstellerWrite(); // \SLIS\Adapter\Hub\Model\HerstellerJsonldWriteHerstellerWrite | The new Hersteller resource

try {
    $result = $apiInstance->postHerstellerCollection($herstellerJsonldWriteHerstellerWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HerstellerApi->postHerstellerCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **herstellerJsonldWriteHerstellerWrite** | [**\SLIS\Adapter\Hub\Model\HerstellerJsonldWriteHerstellerWrite**](../Model/HerstellerJsonldWriteHerstellerWrite.md)| The new Hersteller resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\HerstellerJsonldReadHerstellerRead**](../Model/HerstellerJsonldReadHerstellerRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putHerstellerItem()`

```php
putHerstellerItem($uuid, $herstellerJsonldWriteHerstellerWrite): \SLIS\Adapter\Hub\Model\HerstellerJsonldReadHerstellerRead
```

Replaces the Hersteller resource.

Replaces the Hersteller resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\HerstellerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier
$herstellerJsonldWriteHerstellerWrite = new \SLIS\Adapter\Hub\Model\HerstellerJsonldWriteHerstellerWrite(); // \SLIS\Adapter\Hub\Model\HerstellerJsonldWriteHerstellerWrite | The updated Hersteller resource

try {
    $result = $apiInstance->putHerstellerItem($uuid, $herstellerJsonldWriteHerstellerWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HerstellerApi->putHerstellerItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |
| **herstellerJsonldWriteHerstellerWrite** | [**\SLIS\Adapter\Hub\Model\HerstellerJsonldWriteHerstellerWrite**](../Model/HerstellerJsonldWriteHerstellerWrite.md)| The updated Hersteller resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\HerstellerJsonldReadHerstellerRead**](../Model/HerstellerJsonldReadHerstellerRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
