# SLIS\Adapter\Hub\GeraetApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteGeraetItem()**](GeraetApi.md#deleteGeraetItem) | **DELETE** /geraets/{uuid} | Removes the Geraet resource. |
| [**getGeraetCollection()**](GeraetApi.md#getGeraetCollection) | **GET** /geraets | Retrieves the collection of Geraet resources. |
| [**getGeraetItem()**](GeraetApi.md#getGeraetItem) | **GET** /geraets/{uuid} | Retrieves a Geraet resource. |
| [**putGeraetItem()**](GeraetApi.md#putGeraetItem) | **PUT** /geraets/{uuid} | Replaces the Geraet resource. |


## `deleteGeraetItem()`

```php
deleteGeraetItem($uuid)
```

Removes the Geraet resource.

Removes the Geraet resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteGeraetItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling GeraetApi->deleteGeraetItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getGeraetCollection()`

```php
getGeraetCollection($page, $externalIdentifier, $externalIdentifier2, $ip, $ip2, $mac, $seriennummer, $seriennummer2, $tags, $tags2): \SLIS\Adapter\Hub\Model\GetGeraetCollection200Response
```

Retrieves the collection of Geraet resources.

Retrieves the collection of Geraet resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number
$externalIdentifier = 'externalIdentifier_example'; // string | 
$externalIdentifier2 = array('externalIdentifier_example'); // string[] | 
$ip = 'ip_example'; // string | 
$ip2 = array('ip_example'); // string[] | 
$mac = 'mac_example'; // string | 
$seriennummer = 'seriennummer_example'; // string | 
$seriennummer2 = array('seriennummer_example'); // string[] | 
$tags = 'tags_example'; // string | 
$tags2 = array('tags_example'); // string[] | 

try {
    $result = $apiInstance->getGeraetCollection($page, $externalIdentifier, $externalIdentifier2, $ip, $ip2, $mac, $seriennummer, $seriennummer2, $tags, $tags2);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeraetApi->getGeraetCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |
| **externalIdentifier** | **string**|  | [optional] |
| **externalIdentifier2** | [**string[]**](../Model/string.md)|  | [optional] |
| **ip** | **string**|  | [optional] |
| **ip2** | [**string[]**](../Model/string.md)|  | [optional] |
| **mac** | **string**|  | [optional] |
| **seriennummer** | **string**|  | [optional] |
| **seriennummer2** | [**string[]**](../Model/string.md)|  | [optional] |
| **tags** | **string**|  | [optional] |
| **tags2** | [**string[]**](../Model/string.md)|  | [optional] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetGeraetCollection200Response**](../Model/GetGeraetCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getGeraetItem()`

```php
getGeraetItem($uuid): \SLIS\Adapter\Hub\Model\GeraetJsonldReadGeraetRead
```

Retrieves a Geraet resource.

Retrieves a Geraet resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getGeraetItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeraetApi->getGeraetItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\GeraetJsonldReadGeraetRead**](../Model/GeraetJsonldReadGeraetRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putGeraetItem()`

```php
putGeraetItem($uuid, $geraetJsonldWriteGeraetWrite): \SLIS\Adapter\Hub\Model\GeraetJsonldReadGeraetRead
```

Replaces the Geraet resource.

Replaces the Geraet resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier
$geraetJsonldWriteGeraetWrite = new \SLIS\Adapter\Hub\Model\GeraetJsonldWriteGeraetWrite(); // \SLIS\Adapter\Hub\Model\GeraetJsonldWriteGeraetWrite | The updated Geraet resource

try {
    $result = $apiInstance->putGeraetItem($uuid, $geraetJsonldWriteGeraetWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeraetApi->putGeraetItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |
| **geraetJsonldWriteGeraetWrite** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldWriteGeraetWrite**](../Model/GeraetJsonldWriteGeraetWrite.md)| The updated Geraet resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\GeraetJsonldReadGeraetRead**](../Model/GeraetJsonldReadGeraetRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
