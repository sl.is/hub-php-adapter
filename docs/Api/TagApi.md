# SLIS\Adapter\Hub\TagApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteTagItem()**](TagApi.md#deleteTagItem) | **DELETE** /tags/{uuid} | Removes the Tag resource. |
| [**getTagCollection()**](TagApi.md#getTagCollection) | **GET** /tags | Retrieves the collection of Tag resources. |
| [**getTagItem()**](TagApi.md#getTagItem) | **GET** /tags/{uuid} | Retrieves a Tag resource. |
| [**postTagCollection()**](TagApi.md#postTagCollection) | **POST** /tags | Creates a Tag resource. |
| [**putTagItem()**](TagApi.md#putTagItem) | **PUT** /tags/{uuid} | Replaces the Tag resource. |


## `deleteTagItem()`

```php
deleteTagItem($uuid)
```

Removes the Tag resource.

Removes the Tag resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\TagApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteTagItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling TagApi->deleteTagItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getTagCollection()`

```php
getTagCollection($page): \SLIS\Adapter\Hub\Model\GetTagCollection200Response
```

Retrieves the collection of Tag resources.

Retrieves the collection of Tag resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\TagApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getTagCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagApi->getTagCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetTagCollection200Response**](../Model/GetTagCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getTagItem()`

```php
getTagItem($uuid): \SLIS\Adapter\Hub\Model\TagJsonldReadTagRead
```

Retrieves a Tag resource.

Retrieves a Tag resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\TagApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getTagItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagApi->getTagItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\TagJsonldReadTagRead**](../Model/TagJsonldReadTagRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postTagCollection()`

```php
postTagCollection($tagJsonldWriteTagWrite): \SLIS\Adapter\Hub\Model\TagJsonldReadTagRead
```

Creates a Tag resource.

Creates a Tag resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\TagApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tagJsonldWriteTagWrite = new \SLIS\Adapter\Hub\Model\TagJsonldWriteTagWrite(); // \SLIS\Adapter\Hub\Model\TagJsonldWriteTagWrite | The new Tag resource

try {
    $result = $apiInstance->postTagCollection($tagJsonldWriteTagWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagApi->postTagCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **tagJsonldWriteTagWrite** | [**\SLIS\Adapter\Hub\Model\TagJsonldWriteTagWrite**](../Model/TagJsonldWriteTagWrite.md)| The new Tag resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\TagJsonldReadTagRead**](../Model/TagJsonldReadTagRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putTagItem()`

```php
putTagItem($uuid, $tagJsonldWriteTagWrite): \SLIS\Adapter\Hub\Model\TagJsonldReadTagRead
```

Replaces the Tag resource.

Replaces the Tag resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\TagApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier
$tagJsonldWriteTagWrite = new \SLIS\Adapter\Hub\Model\TagJsonldWriteTagWrite(); // \SLIS\Adapter\Hub\Model\TagJsonldWriteTagWrite | The updated Tag resource

try {
    $result = $apiInstance->putTagItem($uuid, $tagJsonldWriteTagWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TagApi->putTagItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |
| **tagJsonldWriteTagWrite** | [**\SLIS\Adapter\Hub\Model\TagJsonldWriteTagWrite**](../Model/TagJsonldWriteTagWrite.md)| The updated Tag resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\TagJsonldReadTagRead**](../Model/TagJsonldReadTagRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
