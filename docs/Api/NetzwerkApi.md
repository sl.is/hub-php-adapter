# SLIS\Adapter\Hub\NetzwerkApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiStandortsNetzwerkesGetSubresourceStandortSubresource()**](NetzwerkApi.md#apiStandortsNetzwerkesGetSubresourceStandortSubresource) | **GET** /standorts/{uuid}/netzwerkes | Retrieves a Standort resource. |
| [**deleteNetzwerkItem()**](NetzwerkApi.md#deleteNetzwerkItem) | **DELETE** /netzwerk/{uuid} | Removes the Netzwerk resource. |
| [**getNetzwerkCollection()**](NetzwerkApi.md#getNetzwerkCollection) | **GET** /netzwerk | Retrieves the collection of Netzwerk resources. |
| [**getNetzwerkItem()**](NetzwerkApi.md#getNetzwerkItem) | **GET** /netzwerk/{uuid} | Retrieves a Netzwerk resource. |
| [**postNetzwerkCollection()**](NetzwerkApi.md#postNetzwerkCollection) | **POST** /netzwerk | Creates a Netzwerk resource. |


## `apiStandortsNetzwerkesGetSubresourceStandortSubresource()`

```php
apiStandortsNetzwerkesGetSubresourceStandortSubresource($uuid, $page): \SLIS\Adapter\Hub\Model\GetNetzwerkCollection200Response
```

Retrieves a Standort resource.

Retrieves a Standort resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\NetzwerkApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Standort identifier
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->apiStandortsNetzwerkesGetSubresourceStandortSubresource($uuid, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NetzwerkApi->apiStandortsNetzwerkesGetSubresourceStandortSubresource: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Standort identifier | |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetNetzwerkCollection200Response**](../Model/GetNetzwerkCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteNetzwerkItem()`

```php
deleteNetzwerkItem($uuid)
```

Removes the Netzwerk resource.

Removes the Netzwerk resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\NetzwerkApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteNetzwerkItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling NetzwerkApi->deleteNetzwerkItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getNetzwerkCollection()`

```php
getNetzwerkCollection($page): \SLIS\Adapter\Hub\Model\GetNetzwerkCollection200Response
```

Retrieves the collection of Netzwerk resources.

Retrieves the collection of Netzwerk resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\NetzwerkApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getNetzwerkCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NetzwerkApi->getNetzwerkCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetNetzwerkCollection200Response**](../Model/GetNetzwerkCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getNetzwerkItem()`

```php
getNetzwerkItem($uuid): \SLIS\Adapter\Hub\Model\NetzwerkJsonldReadNetzwerkRead
```

Retrieves a Netzwerk resource.

Retrieves a Netzwerk resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\NetzwerkApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getNetzwerkItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NetzwerkApi->getNetzwerkItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\NetzwerkJsonldReadNetzwerkRead**](../Model/NetzwerkJsonldReadNetzwerkRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postNetzwerkCollection()`

```php
postNetzwerkCollection($netzwerkJsonldWriteNetzwerkWrite): \SLIS\Adapter\Hub\Model\NetzwerkJsonldReadNetzwerkRead
```

Creates a Netzwerk resource.

Creates a Netzwerk resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\NetzwerkApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$netzwerkJsonldWriteNetzwerkWrite = new \SLIS\Adapter\Hub\Model\NetzwerkJsonldWriteNetzwerkWrite(); // \SLIS\Adapter\Hub\Model\NetzwerkJsonldWriteNetzwerkWrite | The new Netzwerk resource

try {
    $result = $apiInstance->postNetzwerkCollection($netzwerkJsonldWriteNetzwerkWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NetzwerkApi->postNetzwerkCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **netzwerkJsonldWriteNetzwerkWrite** | [**\SLIS\Adapter\Hub\Model\NetzwerkJsonldWriteNetzwerkWrite**](../Model/NetzwerkJsonldWriteNetzwerkWrite.md)| The new Netzwerk resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\NetzwerkJsonldReadNetzwerkRead**](../Model/NetzwerkJsonldReadNetzwerkRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
