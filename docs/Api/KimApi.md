# SLIS\Adapter\Hub\KimApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteKimItem()**](KimApi.md#deleteKimItem) | **DELETE** /kims/{id} | Removes the Kim resource. |
| [**getKimCollection()**](KimApi.md#getKimCollection) | **GET** /kims | Retrieves the collection of Kim resources. |
| [**getKimItem()**](KimApi.md#getKimItem) | **GET** /kims/{id} | Retrieves a Kim resource. |
| [**patchKimItem()**](KimApi.md#patchKimItem) | **PATCH** /kims/{id} | Updates the Kim resource. |
| [**postKimCollection()**](KimApi.md#postKimCollection) | **POST** /kims | Creates a Kim resource. |
| [**putKimItem()**](KimApi.md#putKimItem) | **PUT** /kims/{id} | Replaces the Kim resource. |


## `deleteKimItem()`

```php
deleteKimItem($id)
```

Removes the Kim resource.

Removes the Kim resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KimApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | Resource identifier

try {
    $apiInstance->deleteKimItem($id);
} catch (Exception $e) {
    echo 'Exception when calling KimApi->deleteKimItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getKimCollection()`

```php
getKimCollection($page): \SLIS\Adapter\Hub\Model\GetKimCollection200Response
```

Retrieves the collection of Kim resources.

Retrieves the collection of Kim resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KimApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getKimCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KimApi->getKimCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetKimCollection200Response**](../Model/GetKimCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getKimItem()`

```php
getKimItem($id): \SLIS\Adapter\Hub\Model\KimJsonld
```

Retrieves a Kim resource.

Retrieves a Kim resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KimApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | Resource identifier

try {
    $result = $apiInstance->getKimItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KimApi->getKimItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\KimJsonld**](../Model/KimJsonld.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `patchKimItem()`

```php
patchKimItem($id, $kim): \SLIS\Adapter\Hub\Model\KimJsonld
```

Updates the Kim resource.

Updates the Kim resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KimApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | Resource identifier
$kim = new \SLIS\Adapter\Hub\Model\Kim(); // \SLIS\Adapter\Hub\Model\Kim | The updated Kim resource

try {
    $result = $apiInstance->patchKimItem($id, $kim);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KimApi->patchKimItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| Resource identifier | |
| **kim** | [**\SLIS\Adapter\Hub\Model\Kim**](../Model/Kim.md)| The updated Kim resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\KimJsonld**](../Model/KimJsonld.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/merge-patch+json`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postKimCollection()`

```php
postKimCollection($kimJsonld): \SLIS\Adapter\Hub\Model\KimJsonld
```

Creates a Kim resource.

Creates a Kim resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KimApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$kimJsonld = new \SLIS\Adapter\Hub\Model\KimJsonld(); // \SLIS\Adapter\Hub\Model\KimJsonld | The new Kim resource

try {
    $result = $apiInstance->postKimCollection($kimJsonld);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KimApi->postKimCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **kimJsonld** | [**\SLIS\Adapter\Hub\Model\KimJsonld**](../Model/KimJsonld.md)| The new Kim resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\KimJsonld**](../Model/KimJsonld.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putKimItem()`

```php
putKimItem($id, $kimJsonld): \SLIS\Adapter\Hub\Model\KimJsonld
```

Replaces the Kim resource.

Replaces the Kim resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KimApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | Resource identifier
$kimJsonld = new \SLIS\Adapter\Hub\Model\KimJsonld(); // \SLIS\Adapter\Hub\Model\KimJsonld | The updated Kim resource

try {
    $result = $apiInstance->putKimItem($id, $kimJsonld);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KimApi->putKimItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| Resource identifier | |
| **kimJsonld** | [**\SLIS\Adapter\Hub\Model\KimJsonld**](../Model/KimJsonld.md)| The updated Kim resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\KimJsonld**](../Model/KimJsonld.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
