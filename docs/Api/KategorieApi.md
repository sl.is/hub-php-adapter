# SLIS\Adapter\Hub\KategorieApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteKategorieItem()**](KategorieApi.md#deleteKategorieItem) | **DELETE** /kategories/{name} | Removes the Kategorie resource. |
| [**getKategorieCollection()**](KategorieApi.md#getKategorieCollection) | **GET** /kategories | Retrieves the collection of Kategorie resources. |
| [**getKategorieItem()**](KategorieApi.md#getKategorieItem) | **GET** /kategories/{name} | Retrieves a Kategorie resource. |
| [**postKategorieCollection()**](KategorieApi.md#postKategorieCollection) | **POST** /kategories | Creates a Kategorie resource. |
| [**putKategorieItem()**](KategorieApi.md#putKategorieItem) | **PUT** /kategories/{name} | Replaces the Kategorie resource. |


## `deleteKategorieItem()`

```php
deleteKategorieItem($name)
```

Removes the Kategorie resource.

Removes the Kategorie resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KategorieApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = 'name_example'; // string | Resource identifier

try {
    $apiInstance->deleteKategorieItem($name);
} catch (Exception $e) {
    echo 'Exception when calling KategorieApi->deleteKategorieItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **name** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getKategorieCollection()`

```php
getKategorieCollection($page, $name): \SLIS\Adapter\Hub\Model\GetKategorieCollection200Response
```

Retrieves the collection of Kategorie resources.

Retrieves the collection of Kategorie resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KategorieApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number
$name = 'name_example'; // string | 

try {
    $result = $apiInstance->getKategorieCollection($page, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KategorieApi->getKategorieCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |
| **name** | **string**|  | [optional] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetKategorieCollection200Response**](../Model/GetKategorieCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getKategorieItem()`

```php
getKategorieItem($name): \SLIS\Adapter\Hub\Model\KategorieJsonldKategorieRead
```

Retrieves a Kategorie resource.

Retrieves a Kategorie resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KategorieApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = 'name_example'; // string | Resource identifier

try {
    $result = $apiInstance->getKategorieItem($name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KategorieApi->getKategorieItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **name** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\KategorieJsonldKategorieRead**](../Model/KategorieJsonldKategorieRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postKategorieCollection()`

```php
postKategorieCollection($kategorieJsonldKategorieWrite): \SLIS\Adapter\Hub\Model\KategorieJsonldKategorieRead
```

Creates a Kategorie resource.

Creates a Kategorie resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KategorieApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$kategorieJsonldKategorieWrite = new \SLIS\Adapter\Hub\Model\KategorieJsonldKategorieWrite(); // \SLIS\Adapter\Hub\Model\KategorieJsonldKategorieWrite | The new Kategorie resource

try {
    $result = $apiInstance->postKategorieCollection($kategorieJsonldKategorieWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KategorieApi->postKategorieCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **kategorieJsonldKategorieWrite** | [**\SLIS\Adapter\Hub\Model\KategorieJsonldKategorieWrite**](../Model/KategorieJsonldKategorieWrite.md)| The new Kategorie resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\KategorieJsonldKategorieRead**](../Model/KategorieJsonldKategorieRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putKategorieItem()`

```php
putKategorieItem($name, $kategorieJsonldKategorieWrite): \SLIS\Adapter\Hub\Model\KategorieJsonldKategorieRead
```

Replaces the Kategorie resource.

Replaces the Kategorie resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KategorieApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = 'name_example'; // string | Resource identifier
$kategorieJsonldKategorieWrite = new \SLIS\Adapter\Hub\Model\KategorieJsonldKategorieWrite(); // \SLIS\Adapter\Hub\Model\KategorieJsonldKategorieWrite | The updated Kategorie resource

try {
    $result = $apiInstance->putKategorieItem($name, $kategorieJsonldKategorieWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KategorieApi->putKategorieItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **name** | **string**| Resource identifier | |
| **kategorieJsonldKategorieWrite** | [**\SLIS\Adapter\Hub\Model\KategorieJsonldKategorieWrite**](../Model/KategorieJsonldKategorieWrite.md)| The updated Kategorie resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\KategorieJsonldKategorieRead**](../Model/KategorieJsonldKategorieRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
