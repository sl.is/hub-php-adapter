# SLIS\Adapter\Hub\BenutzerApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteBenutzerItem()**](BenutzerApi.md#deleteBenutzerItem) | **DELETE** /benutzers/{uuid} | Removes the Benutzer resource. |
| [**getBenutzerCollection()**](BenutzerApi.md#getBenutzerCollection) | **GET** /benutzers | Retrieves the collection of Benutzer resources. |
| [**getBenutzerItem()**](BenutzerApi.md#getBenutzerItem) | **GET** /benutzers/{uuid} | Retrieves a Benutzer resource. |
| [**postBenutzerCollection()**](BenutzerApi.md#postBenutzerCollection) | **POST** /benutzers | Creates a Benutzer resource. |
| [**putBenutzerItem()**](BenutzerApi.md#putBenutzerItem) | **PUT** /benutzers/{uuid} | Replaces the Benutzer resource. |


## `deleteBenutzerItem()`

```php
deleteBenutzerItem($uuid)
```

Removes the Benutzer resource.

Removes the Benutzer resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BenutzerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteBenutzerItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling BenutzerApi->deleteBenutzerItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getBenutzerCollection()`

```php
getBenutzerCollection($page, $properties): \SLIS\Adapter\Hub\Model\GetBenutzerCollection200Response
```

Retrieves the collection of Benutzer resources.

Retrieves the collection of Benutzer resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BenutzerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number
$properties = array('properties_example'); // string[] | 

try {
    $result = $apiInstance->getBenutzerCollection($page, $properties);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BenutzerApi->getBenutzerCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |
| **properties** | [**string[]**](../Model/string.md)|  | [optional] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetBenutzerCollection200Response**](../Model/GetBenutzerCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getBenutzerItem()`

```php
getBenutzerItem($uuid): \SLIS\Adapter\Hub\Model\BenutzerJsonldReadUserRead
```

Retrieves a Benutzer resource.

Retrieves a Benutzer resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BenutzerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getBenutzerItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BenutzerApi->getBenutzerItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\BenutzerJsonldReadUserRead**](../Model/BenutzerJsonldReadUserRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postBenutzerCollection()`

```php
postBenutzerCollection($benutzerJsonldWriteUserWrite): \SLIS\Adapter\Hub\Model\BenutzerJsonldReadUserRead
```

Creates a Benutzer resource.

Creates a Benutzer resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BenutzerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$benutzerJsonldWriteUserWrite = new \SLIS\Adapter\Hub\Model\BenutzerJsonldWriteUserWrite(); // \SLIS\Adapter\Hub\Model\BenutzerJsonldWriteUserWrite | The new Benutzer resource

try {
    $result = $apiInstance->postBenutzerCollection($benutzerJsonldWriteUserWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BenutzerApi->postBenutzerCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **benutzerJsonldWriteUserWrite** | [**\SLIS\Adapter\Hub\Model\BenutzerJsonldWriteUserWrite**](../Model/BenutzerJsonldWriteUserWrite.md)| The new Benutzer resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\BenutzerJsonldReadUserRead**](../Model/BenutzerJsonldReadUserRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putBenutzerItem()`

```php
putBenutzerItem($uuid, $benutzerJsonldWriteUserWrite): \SLIS\Adapter\Hub\Model\BenutzerJsonldReadUserRead
```

Replaces the Benutzer resource.

Replaces the Benutzer resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\BenutzerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier
$benutzerJsonldWriteUserWrite = new \SLIS\Adapter\Hub\Model\BenutzerJsonldWriteUserWrite(); // \SLIS\Adapter\Hub\Model\BenutzerJsonldWriteUserWrite | The updated Benutzer resource

try {
    $result = $apiInstance->putBenutzerItem($uuid, $benutzerJsonldWriteUserWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BenutzerApi->putBenutzerItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |
| **benutzerJsonldWriteUserWrite** | [**\SLIS\Adapter\Hub\Model\BenutzerJsonldWriteUserWrite**](../Model/BenutzerJsonldWriteUserWrite.md)| The updated Benutzer resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\BenutzerJsonldReadUserRead**](../Model/BenutzerJsonldReadUserRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
