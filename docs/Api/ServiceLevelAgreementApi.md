# SLIS\Adapter\Hub\ServiceLevelAgreementApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource()**](ServiceLevelAgreementApi.md#apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource) | **GET** /firewalls/{uuid}/service_level_agreement | Retrieves a Firewall resource. |
| [**deleteServiceLevelAgreementItem()**](ServiceLevelAgreementApi.md#deleteServiceLevelAgreementItem) | **DELETE** /sla/{uuid} | Removes the ServiceLevelAgreement resource. |
| [**getServiceLevelAgreementCollection()**](ServiceLevelAgreementApi.md#getServiceLevelAgreementCollection) | **GET** /sla | Retrieves the collection of ServiceLevelAgreement resources. |
| [**getServiceLevelAgreementItem()**](ServiceLevelAgreementApi.md#getServiceLevelAgreementItem) | **GET** /sla/{uuid} | Retrieves a ServiceLevelAgreement resource. |
| [**postServiceLevelAgreementCollection()**](ServiceLevelAgreementApi.md#postServiceLevelAgreementCollection) | **POST** /sla | Creates a ServiceLevelAgreement resource. |


## `apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource()`

```php
apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource($uuid): \SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead
```

Retrieves a Firewall resource.

Retrieves a Firewall resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\ServiceLevelAgreementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Firewall identifier

try {
    $result = $apiInstance->apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceLevelAgreementApi->apiFirewallsServiceLevelAgreementGetSubresourceFirewallSubresource: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Firewall identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead**](../Model/ServiceLevelAgreementJsonldReadSlaRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteServiceLevelAgreementItem()`

```php
deleteServiceLevelAgreementItem($uuid)
```

Removes the ServiceLevelAgreement resource.

Removes the ServiceLevelAgreement resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\ServiceLevelAgreementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteServiceLevelAgreementItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling ServiceLevelAgreementApi->deleteServiceLevelAgreementItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getServiceLevelAgreementCollection()`

```php
getServiceLevelAgreementCollection($page): \SLIS\Adapter\Hub\Model\GetServiceLevelAgreementCollection200Response
```

Retrieves the collection of ServiceLevelAgreement resources.

Retrieves the collection of ServiceLevelAgreement resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\ServiceLevelAgreementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getServiceLevelAgreementCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceLevelAgreementApi->getServiceLevelAgreementCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetServiceLevelAgreementCollection200Response**](../Model/GetServiceLevelAgreementCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getServiceLevelAgreementItem()`

```php
getServiceLevelAgreementItem($uuid): \SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead
```

Retrieves a ServiceLevelAgreement resource.

Retrieves a ServiceLevelAgreement resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\ServiceLevelAgreementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getServiceLevelAgreementItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceLevelAgreementApi->getServiceLevelAgreementItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead**](../Model/ServiceLevelAgreementJsonldReadSlaRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postServiceLevelAgreementCollection()`

```php
postServiceLevelAgreementCollection($serviceLevelAgreementJsonldWriteSlaWrite): \SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead
```

Creates a ServiceLevelAgreement resource.

Creates a ServiceLevelAgreement resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\ServiceLevelAgreementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$serviceLevelAgreementJsonldWriteSlaWrite = new \SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldWriteSlaWrite(); // \SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldWriteSlaWrite | The new ServiceLevelAgreement resource

try {
    $result = $apiInstance->postServiceLevelAgreementCollection($serviceLevelAgreementJsonldWriteSlaWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceLevelAgreementApi->postServiceLevelAgreementCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **serviceLevelAgreementJsonldWriteSlaWrite** | [**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldWriteSlaWrite**](../Model/ServiceLevelAgreementJsonldWriteSlaWrite.md)| The new ServiceLevelAgreement resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadSlaRead**](../Model/ServiceLevelAgreementJsonldReadSlaRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
