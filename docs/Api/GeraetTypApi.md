# SLIS\Adapter\Hub\GeraetTypApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteGeraetTypItem()**](GeraetTypApi.md#deleteGeraetTypItem) | **DELETE** /geraet-typ/{uuid} | Removes the GeraetTyp resource. |
| [**getGeraetTypCollection()**](GeraetTypApi.md#getGeraetTypCollection) | **GET** /geraet-typ | Retrieves the collection of GeraetTyp resources. |
| [**getGeraetTypItem()**](GeraetTypApi.md#getGeraetTypItem) | **GET** /geraet-typ/{uuid} | Retrieves a GeraetTyp resource. |
| [**postGeraetTypCollection()**](GeraetTypApi.md#postGeraetTypCollection) | **POST** /geraet-typ | Creates a GeraetTyp resource. |


## `deleteGeraetTypItem()`

```php
deleteGeraetTypItem($uuid)
```

Removes the GeraetTyp resource.

Removes the GeraetTyp resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetTypApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteGeraetTypItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling GeraetTypApi->deleteGeraetTypItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getGeraetTypCollection()`

```php
getGeraetTypCollection($page): \SLIS\Adapter\Hub\Model\GetGeraetTypCollection200Response
```

Retrieves the collection of GeraetTyp resources.

Retrieves the collection of GeraetTyp resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetTypApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->getGeraetTypCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeraetTypApi->getGeraetTypCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetGeraetTypCollection200Response**](../Model/GetGeraetTypCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getGeraetTypItem()`

```php
getGeraetTypItem($uuid): \SLIS\Adapter\Hub\Model\GeraetTypJsonldReadGeraetTypRead
```

Retrieves a GeraetTyp resource.

Retrieves a GeraetTyp resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetTypApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getGeraetTypItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeraetTypApi->getGeraetTypItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\GeraetTypJsonldReadGeraetTypRead**](../Model/GeraetTypJsonldReadGeraetTypRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postGeraetTypCollection()`

```php
postGeraetTypCollection($geraetTypJsonldWriteGeraetTypWrite): \SLIS\Adapter\Hub\Model\GeraetTypJsonldReadGeraetTypRead
```

Creates a GeraetTyp resource.

Creates a GeraetTyp resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\GeraetTypApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$geraetTypJsonldWriteGeraetTypWrite = new \SLIS\Adapter\Hub\Model\GeraetTypJsonldWriteGeraetTypWrite(); // \SLIS\Adapter\Hub\Model\GeraetTypJsonldWriteGeraetTypWrite | The new GeraetTyp resource

try {
    $result = $apiInstance->postGeraetTypCollection($geraetTypJsonldWriteGeraetTypWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeraetTypApi->postGeraetTypCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **geraetTypJsonldWriteGeraetTypWrite** | [**\SLIS\Adapter\Hub\Model\GeraetTypJsonldWriteGeraetTypWrite**](../Model/GeraetTypJsonldWriteGeraetTypWrite.md)| The new GeraetTyp resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\GeraetTypJsonldReadGeraetTypRead**](../Model/GeraetTypJsonldReadGeraetTypRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
