# SLIS\Adapter\Hub\KundeApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiKundesBestellungensGetSubresourceKundeSubresource()**](KundeApi.md#apiKundesBestellungensGetSubresourceKundeSubresource) | **GET** /kundes/{uuid}/bestellungens | Retrieves a Kunde resource. |
| [**deleteKundeItem()**](KundeApi.md#deleteKundeItem) | **DELETE** /kunden/{uuid} | Removes the Kunde resource. |
| [**getKundeCollection()**](KundeApi.md#getKundeCollection) | **GET** /kunden | Retrieves the collection of Kunde resources. |
| [**getKundeItem()**](KundeApi.md#getKundeItem) | **GET** /kunden/{uuid} | Retrieves a Kunde resource. |
| [**postKundeCollection()**](KundeApi.md#postKundeCollection) | **POST** /kunden | Creates a Kunde resource. |
| [**putKundeItem()**](KundeApi.md#putKundeItem) | **PUT** /kunden/{uuid} | Replaces the Kunde resource. |


## `apiKundesBestellungensGetSubresourceKundeSubresource()`

```php
apiKundesBestellungensGetSubresourceKundeSubresource($uuid, $page): \SLIS\Adapter\Hub\Model\GetBestellungCollection200Response
```

Retrieves a Kunde resource.

Retrieves a Kunde resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KundeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Kunde identifier
$page = 1; // int | The collection page number

try {
    $result = $apiInstance->apiKundesBestellungensGetSubresourceKundeSubresource($uuid, $page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KundeApi->apiKundesBestellungensGetSubresourceKundeSubresource: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Kunde identifier | |
| **page** | **int**| The collection page number | [optional] [default to 1] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetBestellungCollection200Response**](../Model/GetBestellungCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteKundeItem()`

```php
deleteKundeItem($uuid)
```

Removes the Kunde resource.

Removes the Kunde resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KundeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $apiInstance->deleteKundeItem($uuid);
} catch (Exception $e) {
    echo 'Exception when calling KundeApi->deleteKundeItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getKundeCollection()`

```php
getKundeCollection($page, $kennzeichen, $kennzeichen2): \SLIS\Adapter\Hub\Model\GetKundeCollection200Response
```

Retrieves the collection of Kunde resources.

Retrieves the collection of Kunde resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KundeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | The collection page number
$kennzeichen = 'kennzeichen_example'; // string | 
$kennzeichen2 = array('kennzeichen_example'); // string[] | 

try {
    $result = $apiInstance->getKundeCollection($page, $kennzeichen, $kennzeichen2);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KundeApi->getKundeCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| The collection page number | [optional] [default to 1] |
| **kennzeichen** | **string**|  | [optional] |
| **kennzeichen2** | [**string[]**](../Model/string.md)|  | [optional] |

### Return type

[**\SLIS\Adapter\Hub\Model\GetKundeCollection200Response**](../Model/GetKundeCollection200Response.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getKundeItem()`

```php
getKundeItem($uuid): \SLIS\Adapter\Hub\Model\KundeJsonldReadKundeRead
```

Retrieves a Kunde resource.

Retrieves a Kunde resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KundeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier

try {
    $result = $apiInstance->getKundeItem($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KundeApi->getKundeItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |

### Return type

[**\SLIS\Adapter\Hub\Model\KundeJsonldReadKundeRead**](../Model/KundeJsonldReadKundeRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postKundeCollection()`

```php
postKundeCollection($kundeJsonldWriteKundeWrite): \SLIS\Adapter\Hub\Model\KundeJsonldReadKundeRead
```

Creates a Kunde resource.

Creates a Kunde resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KundeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$kundeJsonldWriteKundeWrite = new \SLIS\Adapter\Hub\Model\KundeJsonldWriteKundeWrite(); // \SLIS\Adapter\Hub\Model\KundeJsonldWriteKundeWrite | The new Kunde resource

try {
    $result = $apiInstance->postKundeCollection($kundeJsonldWriteKundeWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KundeApi->postKundeCollection: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **kundeJsonldWriteKundeWrite** | [**\SLIS\Adapter\Hub\Model\KundeJsonldWriteKundeWrite**](../Model/KundeJsonldWriteKundeWrite.md)| The new Kunde resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\KundeJsonldReadKundeRead**](../Model/KundeJsonldReadKundeRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putKundeItem()`

```php
putKundeItem($uuid, $kundeJsonldWriteKundeWrite): \SLIS\Adapter\Hub\Model\KundeJsonldReadKundeRead
```

Replaces the Kunde resource.

Replaces the Kunde resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SLIS\Adapter\Hub\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new SLIS\Adapter\Hub\Api\KundeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uuid = 'uuid_example'; // string | Resource identifier
$kundeJsonldWriteKundeWrite = new \SLIS\Adapter\Hub\Model\KundeJsonldWriteKundeWrite(); // \SLIS\Adapter\Hub\Model\KundeJsonldWriteKundeWrite | The updated Kunde resource

try {
    $result = $apiInstance->putKundeItem($uuid, $kundeJsonldWriteKundeWrite);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KundeApi->putKundeItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **uuid** | **string**| Resource identifier | |
| **kundeJsonldWriteKundeWrite** | [**\SLIS\Adapter\Hub\Model\KundeJsonldWriteKundeWrite**](../Model/KundeJsonldWriteKundeWrite.md)| The updated Kunde resource | |

### Return type

[**\SLIS\Adapter\Hub\Model\KundeJsonldReadKundeRead**](../Model/KundeJsonldReadKundeRead.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/ld+json`, `application/json`, `text/html`
- **Accept**: `application/ld+json`, `application/json`, `text/html`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
