# # AdresseRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strasse** | **string** |  | [optional]
**plz** | **string** |  | [optional]
**ort** | **string** |  | [optional]
**land** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
