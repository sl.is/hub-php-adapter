# # PositionJsonld

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**id** | **int** |  | [optional] [readonly]
**bestellung** | **string** |  | [optional]
**name** | **string** |  | [optional]
**artikelnummer** | **string** |  | [optional]
**menge** | **int** |  | [optional]
**status** | **string** |  | [optional]
**produkte** | [**\SLIS\Adapter\Hub\Model\ProduktJsonld[]**](ProduktJsonld.md) |  | [optional]
**optionen** | [**\SLIS\Adapter\Hub\Model\PositionOptionJsonld[]**](PositionOptionJsonld.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
