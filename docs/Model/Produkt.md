# # Produkt

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**children** | **string[]** |  | [optional]
**id** | **int** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**position** | [**\SLIS\Adapter\Hub\Model\KimPosition**](KimPosition.md) |  | [optional]
**uuid** | **string** |  | [optional]
**status** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
