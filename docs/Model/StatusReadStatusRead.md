# # StatusReadStatusRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**beschreibung** | **string** |  |
**folgeStatus** | [**\SLIS\Adapter\Hub\Model\StatusReadStatusRead[]**](StatusReadStatusRead.md) |  | [optional]
**type** | **string** |  | [optional]
**farbe** | **string** |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
