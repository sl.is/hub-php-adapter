# # GeraetJsonldReadGeraetRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypJsonldReadGeraetRead**](GeraetTypJsonldReadGeraetRead.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldReadGeraetReadNetzwerk**](GeraetJsonldReadGeraetReadNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldReadGeraetReadHersteller**](GeraetJsonldReadGeraetReadHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagJsonldReadGeraetRead[]**](TagJsonldReadGeraetRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusJsonldReadGeraetRead**](StatusJsonldReadGeraetRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
