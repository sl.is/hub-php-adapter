# # NetzwerkWriteNetzwerkWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**gateway** | **string** |  | [optional]
**standort** | [**\SLIS\Adapter\Hub\Model\StandortWriteNetzwerkWrite**](StandortWriteNetzwerkWrite.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
