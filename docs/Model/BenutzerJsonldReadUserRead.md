# # BenutzerJsonldReadUserRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**email** | **string** |  |
**isMe** | **bool** | Returns true if this is the currently-authenticated user | [optional]
**firstname** | **string** |  | [optional]
**lastname** | **string** |  |
**kunde** | [**\SLIS\Adapter\Hub\Model\BenutzerJsonldReadUserReadKunde**](BenutzerJsonldReadUserReadKunde.md) |  |
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
