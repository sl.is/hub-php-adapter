# # StandortWriteKundeWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseWriteKundeWrite**](AdresseWriteKundeWrite.md) |  | [optional]
**ansprechpartner** | **object[]** |  | [optional]
**kunde** | [**\SLIS\Adapter\Hub\Model\StandortWriteKundeWriteKunde**](StandortWriteKundeWriteKunde.md) |  |
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
