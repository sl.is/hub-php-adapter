# # NetzwerkReadNetzwerkRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**gateway** | **string** |  | [optional]
**geraete** | [**\SLIS\Adapter\Hub\Model\GeraetReadNetzwerkRead[]**](GeraetReadNetzwerkRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusReadNetzwerkRead**](StatusReadNetzwerkRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
