# # GetTagCollection200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hydramember** | [**\SLIS\Adapter\Hub\Model\TagJsonldReadTagRead[]**](TagJsonldReadTagRead.md) |  |
**hydratotalItems** | **int** |  | [optional]
**hydraview** | [**\SLIS\Adapter\Hub\Model\GetBenutzerCollection200ResponseHydraView**](GetBenutzerCollection200ResponseHydraView.md) |  | [optional]
**hydrasearch** | [**\SLIS\Adapter\Hub\Model\GetBenutzerCollection200ResponseHydraSearch**](GetBenutzerCollection200ResponseHydraSearch.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
