# # BestellungRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kunde** | [**\SLIS\Adapter\Hub\Model\KundeRead**](KundeRead.md) |  | [optional]
**standort** | [**\SLIS\Adapter\Hub\Model\StandortRead**](StandortRead.md) |  | [optional]
**nummer** | **string** |  | [optional]
**positionen** | [**\SLIS\Adapter\Hub\Model\PositionRead[]**](PositionRead.md) |  | [optional]
**ersteller** | [**\SLIS\Adapter\Hub\Model\BenutzerRead**](BenutzerRead.md) |  | [optional]
**webhook** | [**\SLIS\Adapter\Hub\Model\BestellungReadWebhook**](BestellungReadWebhook.md) |  | [optional]
**audit** | [**\SLIS\Adapter\Hub\Model\StatusLogRead[]**](StatusLogRead.md) |  | [optional] [readonly]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusRead**](StatusRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
