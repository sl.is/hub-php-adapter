# # StatusLogRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**datum** | **\DateTime** |  | [optional] [readonly]
**status** | [**\SLIS\Adapter\Hub\Model\StatusRead**](StatusRead.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
