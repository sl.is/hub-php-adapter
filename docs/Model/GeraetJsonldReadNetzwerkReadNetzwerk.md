# # GeraetJsonldReadNetzwerkReadNetzwerk

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**name** | **string** |  | [optional]
**gateway** | **string** |  | [optional]
**geraete** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldReadNetzwerkRead[]**](GeraetJsonldReadNetzwerkRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusJsonldReadNetzwerkRead**](StatusJsonldReadNetzwerkRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
