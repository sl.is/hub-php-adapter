# # BestellungPatch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook** | [**\SLIS\Adapter\Hub\Model\BestellungPatchWebhook**](BestellungPatchWebhook.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
