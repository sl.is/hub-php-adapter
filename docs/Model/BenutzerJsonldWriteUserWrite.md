# # BenutzerJsonldWriteUserWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**email** | **string** |  |
**password** | **string** |  | [optional]
**firstname** | **string** |  | [optional]
**lastname** | **string** |  |
**kunde** | [**\SLIS\Adapter\Hub\Model\BenutzerJsonldWriteUserWriteKunde**](BenutzerJsonldWriteUserWriteKunde.md) |  |
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
