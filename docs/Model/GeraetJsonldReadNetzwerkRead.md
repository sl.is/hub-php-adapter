# # GeraetJsonldReadNetzwerkRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypJsonldReadNetzwerkRead**](GeraetTypJsonldReadNetzwerkRead.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldReadNetzwerkReadNetzwerk**](GeraetJsonldReadNetzwerkReadNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldReadNetzwerkReadHersteller**](GeraetJsonldReadNetzwerkReadHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagJsonldReadNetzwerkRead[]**](TagJsonldReadNetzwerkRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusJsonldReadNetzwerkRead**](StatusJsonldReadNetzwerkRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
