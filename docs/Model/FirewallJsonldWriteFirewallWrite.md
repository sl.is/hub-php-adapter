# # FirewallJsonldWriteFirewallWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**serviceLevelAgreement** | [**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldWriteFirewallWrite**](ServiceLevelAgreementJsonldWriteFirewallWrite.md) |  | [optional]
**uuid** | **string** |  | [optional]
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypJsonldWriteFirewallWrite**](GeraetTypJsonldWriteFirewallWrite.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWriteNetzwerk**](FirewallJsonldWriteFirewallWriteNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\FirewallJsonldWriteFirewallWriteHersteller**](FirewallJsonldWriteFirewallWriteHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagJsonldWriteFirewallWrite[]**](TagJsonldWriteFirewallWrite.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
