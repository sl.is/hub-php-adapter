# # ProduktJsonld

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**children** | **string[]** |  | [optional]
**id** | **int** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**position** | [**\SLIS\Adapter\Hub\Model\KimJsonldPosition**](KimJsonldPosition.md) |  | [optional]
**uuid** | **string** |  | [optional]
**status** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
