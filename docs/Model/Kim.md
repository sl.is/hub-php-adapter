# # Kim

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**initialStatus** | **string** |  | [optional]
**type** | **string** |  | [optional] [default to 'KIM']
**bestellungsposition** | **string[]** |  | [optional]
**kimContractId** | **string** |  | [optional]
**kimAdresse** | **string** |  | [optional]
**passwort** | **string** |  | [optional]
**zertifikatsnummer** | **string** |  | [optional]
**children** | **string[]** |  | [optional]
**uuid** | **string** |  | [optional]
**id** | **int** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**position** | [**\SLIS\Adapter\Hub\Model\KimPosition**](KimPosition.md) |  | [optional]
**status** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
