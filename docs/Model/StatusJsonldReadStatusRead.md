# # StatusJsonldReadStatusRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**name** | **string** |  |
**beschreibung** | **string** |  |
**folgeStatus** | [**\SLIS\Adapter\Hub\Model\StatusJsonldReadStatusRead[]**](StatusJsonldReadStatusRead.md) |  | [optional]
**type** | **string** |  | [optional]
**farbe** | **string** |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
