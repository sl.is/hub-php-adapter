# # StandortWriteKundeWriteKunde

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**nummer** | **string** |  | [optional]
**kennzeichen** | **string** |  | [optional]
**bsnr** | **int** |  | [optional]
**institutionskennzeichen** | **int** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseWriteKundeWrite**](AdresseWriteKundeWrite.md) |  | [optional]
**rechungsadresse** | [**\SLIS\Adapter\Hub\Model\KundeWriteKundeWriteRechungsadresse**](KundeWriteKundeWriteRechungsadresse.md) |  | [optional]
**standorte** | [**\SLIS\Adapter\Hub\Model\StandortWriteKundeWrite[]**](StandortWriteKundeWrite.md) |  | [optional]
**kategorien** | **string[]** |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
