# # KundeCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**nummer** | **string** |  | [optional]
**kennzeichen** | **string** |  | [optional]
**bsnr** | **int** |  | [optional]
**institutionskennzeichen** | **int** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseCreate**](AdresseCreate.md) |  | [optional]
**rechungsadresse** | [**\SLIS\Adapter\Hub\Model\KundeCreateRechungsadresse**](KundeCreateRechungsadresse.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
