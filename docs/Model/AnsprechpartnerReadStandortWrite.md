# # AnsprechpartnerReadStandortWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**standard** | **bool** |  | [optional] [default to true]
**geburtsdatum** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
