# # KimPosition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] [readonly]
**bestellung** | **string** |  | [optional]
**name** | **string** |  | [optional]
**artikelnummer** | **string** |  | [optional]
**menge** | **int** |  | [optional]
**status** | **string** |  | [optional]
**produkte** | [**\SLIS\Adapter\Hub\Model\Produkt[]**](Produkt.md) |  | [optional]
**optionen** | [**\SLIS\Adapter\Hub\Model\PositionOption[]**](PositionOption.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
