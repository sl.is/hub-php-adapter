# # StandortJsonldReadKundeRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldReadKundeRead**](AdresseJsonldReadKundeRead.md) |  | [optional]
**ansprechpartner** | [**\SLIS\Adapter\Hub\Model\AnsprechpartnerJsonldReadKundeRead[]**](AnsprechpartnerJsonldReadKundeRead.md) |  | [optional]
**netzwerke** | [**\SLIS\Adapter\Hub\Model\NetzwerkJsonldReadKundeRead[]**](NetzwerkJsonldReadKundeRead.md) |  | [optional]
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
