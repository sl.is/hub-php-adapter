# # StandortReadKundeRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseReadKundeRead**](AdresseReadKundeRead.md) |  | [optional]
**ansprechpartner** | [**\SLIS\Adapter\Hub\Model\AnsprechpartnerReadKundeRead[]**](AnsprechpartnerReadKundeRead.md) |  | [optional]
**netzwerke** | [**\SLIS\Adapter\Hub\Model\NetzwerkReadKundeRead[]**](NetzwerkReadKundeRead.md) |  | [optional]
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
