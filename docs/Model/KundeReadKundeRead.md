# # KundeReadKundeRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**nummer** | **string** |  | [optional]
**kennzeichen** | **string** |  | [optional]
**bsnr** | **int** |  | [optional]
**institutionskennzeichen** | **int** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseReadKundeRead**](AdresseReadKundeRead.md) |  | [optional]
**rechungsadresse** | [**\SLIS\Adapter\Hub\Model\KundeReadKundeReadRechungsadresse**](KundeReadKundeReadRechungsadresse.md) |  | [optional]
**standorte** | [**\SLIS\Adapter\Hub\Model\StandortReadKundeRead[]**](StandortReadKundeRead.md) |  | [optional]
**benutzer** | [**\SLIS\Adapter\Hub\Model\BenutzerReadKundeRead[]**](BenutzerReadKundeRead.md) |  | [optional]
**kategorien** | **string[]** |  | [optional]
**bestellungen** | [**\SLIS\Adapter\Hub\Model\BestellungReadKundeRead[]**](BestellungReadKundeRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
