# # BestellungCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kunde** | [**\SLIS\Adapter\Hub\Model\KundeCreate**](KundeCreate.md) |  | [optional]
**standort** | [**\SLIS\Adapter\Hub\Model\StandortCreate**](StandortCreate.md) |  | [optional]
**positionen** | [**\SLIS\Adapter\Hub\Model\PositionCreate[]**](PositionCreate.md) |  | [optional]
**webhook** | [**\SLIS\Adapter\Hub\Model\BestellungCreateWebhook**](BestellungCreateWebhook.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
