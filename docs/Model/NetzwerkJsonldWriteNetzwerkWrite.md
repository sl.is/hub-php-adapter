# # NetzwerkJsonldWriteNetzwerkWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**gateway** | **string** |  | [optional]
**standort** | [**\SLIS\Adapter\Hub\Model\StandortJsonldWriteNetzwerkWrite**](StandortJsonldWriteNetzwerkWrite.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
