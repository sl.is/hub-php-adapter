# # FirewallWriteFirewallWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serviceLevelAgreement** | [**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementWriteFirewallWrite**](ServiceLevelAgreementWriteFirewallWrite.md) |  | [optional]
**uuid** | **string** |  | [optional]
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypWriteFirewallWrite**](GeraetTypWriteFirewallWrite.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\FirewallWriteFirewallWriteNetzwerk**](FirewallWriteFirewallWriteNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\FirewallWriteFirewallWriteHersteller**](FirewallWriteFirewallWriteHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagWriteFirewallWrite[]**](TagWriteFirewallWrite.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
