# # StandortCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseCreate**](AdresseCreate.md) |  | [optional]
**ansprechpartner** | [**\SLIS\Adapter\Hub\Model\AnsprechpartnerCreate[]**](AnsprechpartnerCreate.md) |  | [optional]
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
