# # GeraetJsonldWriteGeraetWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypJsonldWriteGeraetWrite**](GeraetTypJsonldWriteGeraetWrite.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldWriteGeraetWriteNetzwerk**](GeraetJsonldWriteGeraetWriteNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\GeraetJsonldWriteGeraetWriteHersteller**](GeraetJsonldWriteGeraetWriteHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagJsonldWriteGeraetWrite[]**](TagJsonldWriteGeraetWrite.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
