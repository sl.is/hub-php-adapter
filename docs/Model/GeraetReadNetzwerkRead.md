# # GeraetReadNetzwerkRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypReadNetzwerkRead**](GeraetTypReadNetzwerkRead.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\GeraetReadNetzwerkReadNetzwerk**](GeraetReadNetzwerkReadNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\GeraetReadNetzwerkReadHersteller**](GeraetReadNetzwerkReadHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagReadNetzwerkRead[]**](TagReadNetzwerkRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusReadNetzwerkRead**](StatusReadNetzwerkRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
