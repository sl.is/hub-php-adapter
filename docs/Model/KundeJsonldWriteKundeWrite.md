# # KundeJsonldWriteKundeWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**name** | **string** |  |
**nummer** | **string** |  | [optional]
**kennzeichen** | **string** |  | [optional]
**bsnr** | **int** |  | [optional]
**institutionskennzeichen** | **int** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldWriteKundeWrite**](AdresseJsonldWriteKundeWrite.md) |  | [optional]
**rechungsadresse** | [**\SLIS\Adapter\Hub\Model\KundeJsonldWriteKundeWriteRechungsadresse**](KundeJsonldWriteKundeWriteRechungsadresse.md) |  | [optional]
**standorte** | [**\SLIS\Adapter\Hub\Model\StandortJsonldWriteKundeWrite[]**](StandortJsonldWriteKundeWrite.md) |  | [optional]
**kategorien** | **string[]** |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
