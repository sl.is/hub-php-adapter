# # FirewallJsonldReadFirewallRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**serviceLevelAgreement** | [**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementJsonldReadFirewallRead**](ServiceLevelAgreementJsonldReadFirewallRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypJsonldReadFirewallRead**](GeraetTypJsonldReadFirewallRead.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallReadNetzwerk**](FirewallJsonldReadFirewallReadNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\FirewallJsonldReadFirewallReadHersteller**](FirewallJsonldReadFirewallReadHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagJsonldReadFirewallRead[]**](TagJsonldReadFirewallRead.md) |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusJsonldReadFirewallRead**](StatusJsonldReadFirewallRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
