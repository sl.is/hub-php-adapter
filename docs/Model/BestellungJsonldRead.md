# # BestellungJsonldRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**kunde** | [**\SLIS\Adapter\Hub\Model\KundeJsonldRead**](KundeJsonldRead.md) |  | [optional]
**standort** | [**\SLIS\Adapter\Hub\Model\StandortJsonldRead**](StandortJsonldRead.md) |  | [optional]
**nummer** | **string** |  | [optional]
**positionen** | [**\SLIS\Adapter\Hub\Model\PositionJsonldRead[]**](PositionJsonldRead.md) |  | [optional]
**ersteller** | [**\SLIS\Adapter\Hub\Model\BenutzerJsonldRead**](BenutzerJsonldRead.md) |  | [optional]
**webhook** | [**\SLIS\Adapter\Hub\Model\BestellungJsonldReadWebhook**](BestellungJsonldReadWebhook.md) |  | [optional]
**audit** | [**\SLIS\Adapter\Hub\Model\StatusLogJsonldRead[]**](StatusLogJsonldRead.md) |  | [optional] [readonly]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusJsonldRead**](StatusJsonldRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
