# # StandortReadStandortWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseReadStandortWrite**](AdresseReadStandortWrite.md) |  | [optional]
**ansprechpartner** | [**\SLIS\Adapter\Hub\Model\AnsprechpartnerReadStandortWrite[]**](AnsprechpartnerReadStandortWrite.md) |  | [optional]
**netzwerke** | [**\SLIS\Adapter\Hub\Model\NetzwerkReadStandortWrite[]**](NetzwerkReadStandortWrite.md) |  | [optional]
**kunde** | [**\SLIS\Adapter\Hub\Model\StandortReadStandortWriteKunde**](StandortReadStandortWriteKunde.md) |  |
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
