# # GeraetWriteGeraetWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypWriteGeraetWrite**](GeraetTypWriteGeraetWrite.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\GeraetWriteGeraetWriteNetzwerk**](GeraetWriteGeraetWriteNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\GeraetWriteGeraetWriteHersteller**](GeraetWriteGeraetWriteHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagWriteGeraetWrite[]**](TagWriteGeraetWrite.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
