# # BenutzerWriteUserWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  |
**password** | **string** |  | [optional]
**firstname** | **string** |  | [optional]
**lastname** | **string** |  |
**kunde** | [**\SLIS\Adapter\Hub\Model\BenutzerWriteUserWriteKunde**](BenutzerWriteUserWriteKunde.md) |  |
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
