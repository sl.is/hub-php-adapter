# # ServiceLevelAgreementWriteSlaWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**konfiguration** | [**\SLIS\Adapter\Hub\Model\KonfigurationWriteSlaWrite[]**](KonfigurationWriteSlaWrite.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
