# # AnsprechpartnerJsonldCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**anrede** | **string** |  | [optional]
**vorname** | **string** |  | [optional]
**nachname** | **string** |  | [optional]
**email** | **string** |  | [optional]
**telefon** | **string** |  | [optional]
**geburtsdatum** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
