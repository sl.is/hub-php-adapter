# # StandortJsonldReadStandortWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldReadStandortWrite**](AdresseJsonldReadStandortWrite.md) |  | [optional]
**ansprechpartner** | [**\SLIS\Adapter\Hub\Model\AnsprechpartnerJsonldReadStandortWrite[]**](AnsprechpartnerJsonldReadStandortWrite.md) |  | [optional]
**netzwerke** | [**\SLIS\Adapter\Hub\Model\NetzwerkJsonldReadStandortWrite[]**](NetzwerkJsonldReadStandortWrite.md) |  | [optional]
**kunde** | [**\SLIS\Adapter\Hub\Model\StandortJsonldReadStandortWriteKunde**](StandortJsonldReadStandortWriteKunde.md) |  |
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
