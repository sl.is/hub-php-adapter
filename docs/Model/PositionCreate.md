# # PositionCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**artikelnummer** | **string** |  | [optional]
**menge** | **int** |  | [optional]
**optionen** | [**\SLIS\Adapter\Hub\Model\PositionOptionCreate[]**](PositionOptionCreate.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
