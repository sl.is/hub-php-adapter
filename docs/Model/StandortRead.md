# # StandortRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseRead**](AdresseRead.md) |  | [optional]
**ansprechpartner** | [**\SLIS\Adapter\Hub\Model\AnsprechpartnerRead[]**](AnsprechpartnerRead.md) |  | [optional]
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
