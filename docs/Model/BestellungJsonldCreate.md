# # BestellungJsonldCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**kunde** | [**\SLIS\Adapter\Hub\Model\KundeJsonldCreate**](KundeJsonldCreate.md) |  | [optional]
**standort** | [**\SLIS\Adapter\Hub\Model\StandortJsonldCreate**](StandortJsonldCreate.md) |  | [optional]
**positionen** | [**\SLIS\Adapter\Hub\Model\PositionJsonldCreate[]**](PositionJsonldCreate.md) |  | [optional]
**webhook** | [**\SLIS\Adapter\Hub\Model\BestellungJsonldCreateWebhook**](BestellungJsonldCreateWebhook.md) |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
