# # BenutzerReadUserRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  |
**isMe** | **bool** | Returns true if this is the currently-authenticated user | [optional]
**firstname** | **string** |  | [optional]
**lastname** | **string** |  |
**kunde** | [**\SLIS\Adapter\Hub\Model\BenutzerReadUserReadKunde**](BenutzerReadUserReadKunde.md) |  |
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
