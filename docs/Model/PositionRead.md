# # PositionRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**artikelnummer** | **string** |  | [optional]
**menge** | **int** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\PositionReadStatus**](PositionReadStatus.md) |  | [optional]
**produkte** | [**\SLIS\Adapter\Hub\Model\ProduktRead[]**](ProduktRead.md) |  | [optional]
**optionen** | [**\SLIS\Adapter\Hub\Model\PositionOptionRead[]**](PositionOptionRead.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
