# # PositionOptionJsonld

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**id** | **int** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**wert** | **string** |  | [optional]
**position** | [**\SLIS\Adapter\Hub\Model\PositionJsonld**](PositionJsonld.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
