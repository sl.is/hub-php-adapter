# # FirewallReadFirewallRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serviceLevelAgreement** | [**\SLIS\Adapter\Hub\Model\ServiceLevelAgreementReadFirewallRead**](ServiceLevelAgreementReadFirewallRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**name** | **string** |  | [optional]
**ip** | **string** |  | [optional]
**mac** | **string** |  | [optional]
**typ** | [**\SLIS\Adapter\Hub\Model\GeraetTypReadFirewallRead**](GeraetTypReadFirewallRead.md) |  | [optional]
**beschreibung** | **string** |  | [optional]
**seriennummer** | **string** |  | [optional]
**version** | **string** |  | [optional]
**externalIdentifier** | **string** |  | [optional]
**netzwerk** | [**\SLIS\Adapter\Hub\Model\FirewallReadFirewallReadNetzwerk**](FirewallReadFirewallReadNetzwerk.md) |  | [optional]
**hersteller** | [**\SLIS\Adapter\Hub\Model\FirewallReadFirewallReadHersteller**](FirewallReadFirewallReadHersteller.md) |  | [optional]
**tags** | [**\SLIS\Adapter\Hub\Model\TagReadFirewallRead[]**](TagReadFirewallRead.md) |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusReadFirewallRead**](StatusReadFirewallRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
