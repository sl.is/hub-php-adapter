# # BestellungReadKundeRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audit** | [**\SLIS\Adapter\Hub\Model\StatusLogReadKundeRead[]**](StatusLogReadKundeRead.md) |  | [optional] [readonly]
**uuid** | **string** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\StatusReadKundeRead**](StatusReadKundeRead.md) |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
