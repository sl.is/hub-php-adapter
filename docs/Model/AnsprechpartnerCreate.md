# # AnsprechpartnerCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**anrede** | **string** |  | [optional]
**vorname** | **string** |  | [optional]
**nachname** | **string** |  | [optional]
**email** | **string** |  | [optional]
**telefon** | **string** |  | [optional]
**geburtsdatum** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
