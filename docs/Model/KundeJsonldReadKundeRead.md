# # KundeJsonldReadKundeRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**name** | **string** |  |
**nummer** | **string** |  | [optional]
**kennzeichen** | **string** |  | [optional]
**bsnr** | **int** |  | [optional]
**institutionskennzeichen** | **int** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldReadKundeRead**](AdresseJsonldReadKundeRead.md) |  | [optional]
**rechungsadresse** | [**\SLIS\Adapter\Hub\Model\KundeJsonldReadKundeReadRechungsadresse**](KundeJsonldReadKundeReadRechungsadresse.md) |  | [optional]
**standorte** | [**\SLIS\Adapter\Hub\Model\StandortJsonldReadKundeRead[]**](StandortJsonldReadKundeRead.md) |  | [optional]
**benutzer** | [**\SLIS\Adapter\Hub\Model\BenutzerJsonldReadKundeRead[]**](BenutzerJsonldReadKundeRead.md) |  | [optional]
**kategorien** | **string[]** |  | [optional]
**bestellungen** | [**\SLIS\Adapter\Hub\Model\BestellungJsonldReadKundeRead[]**](BestellungJsonldReadKundeRead.md) |  | [optional]
**uuid** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional] [readonly]
**updated** | **\DateTime** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
