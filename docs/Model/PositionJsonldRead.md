# # PositionJsonldRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**name** | **string** |  | [optional]
**artikelnummer** | **string** |  | [optional]
**menge** | **int** |  | [optional]
**status** | [**\SLIS\Adapter\Hub\Model\PositionJsonldReadStatus**](PositionJsonldReadStatus.md) |  | [optional]
**produkte** | [**\SLIS\Adapter\Hub\Model\ProduktJsonldRead[]**](ProduktJsonldRead.md) |  | [optional]
**optionen** | [**\SLIS\Adapter\Hub\Model\PositionOptionJsonldRead[]**](PositionOptionJsonldRead.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
