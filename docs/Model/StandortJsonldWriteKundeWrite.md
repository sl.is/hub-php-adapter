# # StandortJsonldWriteKundeWrite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atContext** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldCreateContext**](AdresseJsonldCreateContext.md) |  | [optional]
**atId** | **string** |  | [optional] [readonly]
**atType** | **string** |  | [optional] [readonly]
**telefon** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**email** | **string** |  | [optional]
**adresse** | [**\SLIS\Adapter\Hub\Model\AdresseJsonldWriteKundeWrite**](AdresseJsonldWriteKundeWrite.md) |  | [optional]
**ansprechpartner** | [**\SLIS\Adapter\Hub\Model\AnsprechpartnerJsonldWriteKundeWrite[]**](AnsprechpartnerJsonldWriteKundeWrite.md) |  | [optional]
**kunde** | [**\SLIS\Adapter\Hub\Model\StandortJsonldWriteKundeWriteKunde**](StandortJsonldWriteKundeWriteKunde.md) |  |
**hauptsitz** | **bool** |  | [optional]
**uuid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
