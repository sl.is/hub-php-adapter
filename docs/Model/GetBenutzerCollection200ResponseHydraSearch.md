# # GetBenutzerCollection200ResponseHydraSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atType** | **string** |  | [optional]
**hydratemplate** | **string** |  | [optional]
**hydravariableRepresentation** | **string** |  | [optional]
**hydramapping** | [**\SLIS\Adapter\Hub\Model\GetBenutzerCollection200ResponseHydraSearchHydraMappingInner[]**](GetBenutzerCollection200ResponseHydraSearchHydraMappingInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
